# Prometheus Docker-Compose monitoring

Темплейт для развертывания мониторинга, постоянные изменения на месте, как развернем свой гит этот репозиторий переедет

Структура
=========

1. alertmanager
⋅ config.yml - настройка перенаправления оповещений к телеграм боту
2. grafana
    * provisioning
        + dashboards
            - dashboard.yml - "указатель" пути для графаны, откуда брать дашбоарды
            - docker_containers.json - панель мониторинга запущенных контейнеров
            - docker_host.json - панель мониторинга докера
            - grafana_metrics_old.json - панель мониторинга ресурсов графаны (да, мониторинг мониторит себя)
            - monitor_services.json - панель для монитонга нагрузки сервисов
            - nginx_container.json - панель мониторинга nginx серверов
            - node_exporter_full_old.json - панель мониторинга нагрузки на сервер
            - prom_2_old.json - мониторинг монитоинга)
            - tgmu_node_1_old.json - панель мониторинга нагрузки на старый сервер?
            - wmi_exporter.json - панель мониторинга для windows серверов
        + datasources
            - datasource.yml - "указатель" для графаны где прометеус
3. prometheus
    * alert.rules - правила оповещения для прометеуса
    * prometheus.yml - конфигурация прометеуса
4. docker-compose.exporters.yml - используемые экспортеры
5. docker-compose.yml - запуск стека мониторинга

![Схема мониторинга](https://gitlab.com/skynext72/docker-grafana-promet-alert-psql/-/blob/main/MonitoringDiagram.jpg?raw=true "Схема мониторинга")

++++++++++
Примечания
++++++++++

Все панели dashboards, в названии которых есть приставка old в названии перенесены с предыдущей версии мониторинга.
